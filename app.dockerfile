FROM php:fpm

RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libzip-dev \
        zip \
    && docker-php-ext-configure zip --with-libzip \
    && apt-get update && apt-get install -y zlib1g-dev \
    && docker-php-ext-install zip bcmath pdo_mysql mysqli \
    && apt-get remove -y libfreetype6-dev libjpeg62-turbo-dev libpng-dev libzip-dev zlib1g-dev zip

COPY docker.start.sh /usr/local/bin/start
COPY docker.php.ini $PHP_INI_DIR/php.ini

COPY ./ /var/www/html
#RUN rm /var/www/html/bootstrap/cache/*

#RUN chown -R www-data:www-data /var/www && chmod 755 /var/www && chmod u+x /usr/local/bin/start \
#    && apt-get clean
RUN chmod 755 /var/www && chmod u+x /usr/local/bin/start && apt-get clean

WORKDIR /var/www/html

CMD ["/usr/local/bin/start"]