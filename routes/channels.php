<?php

Broadcast::channel('user.{id}', \App\Broadcasting\UserChannel::class);

Broadcast::channel('TwitchUserFollowsEvent.{twitchId}', \App\Broadcasting\TwitchEventChannel::class);

Broadcast::channel('TwitchStreamChangedEvent.{twitchId}', \App\Broadcasting\TwitchEventChannel::class);
