<?php

Route::get('/', 'IndexController');
Route::get('/stream/subscribe', 'StreamController@subscribe')->name('stream.subscribe');
Route::post('/stream/watch', 'StreamController@index')->name('stream.index');

Route::get('login/{provider}', 'SocialController@redirectToProvider')
    ->name('social.auth.login');

Route::get('login/{provider}/callback', 'SocialController@handleProviderCallback')
    ->name('social.auth.callback');