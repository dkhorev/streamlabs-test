<?php

Route::group(['prefix' => 'in/events'], function () {
    Route::post('user-follows/{twitchId}', 'TwitchEventsController@userFollows')
         ->name('in.events.user-follows');
    Route::get('user-follows/{twitchId}', 'TwitchEventsController@userFollows')
         ->name('in.events.user-follows');

    Route::post('stream-changed/{twitchId}', 'TwitchEventsController@streamChanged')
         ->name('in.events.stream-changed');
    Route::get('stream-changed/{twitchId}', 'TwitchEventsController@streamChanged')
         ->name('in.events.stream-changed');
});
