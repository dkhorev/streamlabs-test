FROM nginx:alpine

COPY ./public /var/www/html/public
COPY web.prod.conf /etc/nginx/conf.d/default.conf

WORKDIR /var/www/html