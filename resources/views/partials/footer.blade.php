<footer class="pt-4">
    <div class="footer-copyright text-center py-3">
        {{ config('app.name') }} © {{ date('Y') }}
    </div>
</footer>
