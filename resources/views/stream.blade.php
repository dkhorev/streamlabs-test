@extends('layouts.app')

@section('title', config('app.name'))

@section('content')
    <section id="app" class="container">
        <h1>{{ $username }}'s stream</h1>
        <div class="row">
            <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-12">
                        <iframe
                                src="https://player.twitch.tv/?channel={{ $username }}&muted=true"
                                height="480"
                                width="800"
                                frameborder="0"
                                scrolling="no"
                                allowfullscreen="true">
                        </iframe>
                    </div>
                    <div class="col-sm-12">
                        <iframe frameborder="0"
                                scrolling="no"
                                id="chat_embed"
                                src="https://www.twitch.tv/embed/{{ $username }}/chat"
                                height="400"
                                width="800">
                        </iframe>
                    </div>
                </div>
            </div>
            <div class="col-sm-3" style="max-height: 600px">
                <div class="row">
                    <div class="col-sm-12" style="min-height: 400px">
                        <span id="status">
                            <div class="alert alert-dismissible alert-warning">
                                <p class="mb-0">Subscribing...</p>
                            </div>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <template id="eventTemplate">
        <div class="alert alert-dismissible alert-success">
            <p class="mb-0">:text</p>
        </div>
    </template>
@endsection

@section('scripts')
    <script src="{{ mix('/js/manifest.js') }}"></script>
    <script src="{{ mix('/js/vendor.js') }}" defer></script>
    <script src="{{ mix('/js/app.js') }}" defer></script>

    <script>
    {{-- todo simplified event listening outside app.js--}}
    let template = document.querySelector('#eventTemplate').innerHTML
    let eventList = document.querySelector('#status')

    function addEventToList (text) {
      eventList.innerHTML = template.replace(':text', text) + eventList.innerHTML
    }

    document.addEventListener('DOMContentLoaded', function () {


      Echo.private('user.{{ $user_id }}').listen('.App\\Events\\Twitch\\StreamerIdReceived', (e) => {
        console.log('StreamerIdReceived', e.twitchId)
        addEventToList('Ok, waiting for events')

        Echo.leave('user.{{ $user_id }}')

        Echo.channel('TwitchUserFollowsEvent.' + e.twitchId).listen('.App\\Events\\Twitch\\UserFollowsEventReceived', (e) => {
          console.log('UserFollowsEventReceived', e)
          addEventToList('UserFollows: ' + e.data.from_name)
        })

        Echo.channel('TwitchStreamChangedEvent.' + e.twitchId).listen('.App\\Events\\Twitch\\StreamChangedEventReceived', (e) => {
          console.log('StreamChangedEventReceived', e)
          addEventToList('StreamChanged: ' + e.data.user_name)
        })

      })
    })
    </script>
@endsection

