@extends('layouts.app')

@section('title', config('app.name'))

@section('content')
    <section id="app" class="container">
        @if(!empty($errors->first()))
            <div class="row col-lg-12 mt-3">
                <div class="alert alert-danger">
                    <span>{{ $errors->first() }}</span>
                </div>
            </div>
        @endif

        <form method="POST" action="{{ route('stream.index') }}">
            @csrf
            <div class="form-group">
                <label class="control-label">Type favourite streamer name</label>
                <div class="form-group">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">@</span>
                        </div>
                        <input type="text" name="username" class="form-control" aria-label="streamer name">
                    </div>
                    <button type="submit" class="btn btn-success btn-lg">Watch stream</button>
                </div>
            </div>
        </form>
    </section>
@endsection


