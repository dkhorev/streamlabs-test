@extends('layouts.app')

@section('title', config('app.name'))

@section('content')
    <section id="app" class="container">
        <div class="row mt-5">
            <a class="btn btn-primary btn-lg btn-block" href="{{ route('social.auth.login', ['provider' => 'twitch']) }}">Login with Twitch</a>
        </div>
    </section>
@endsection


