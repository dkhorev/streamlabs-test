<?php

namespace App\Providers;

use App\Contracts\Auth\TwitchLoginServiceContract;
use App\Contracts\Auth\TwitchRegisterServiceConract;
use App\Contracts\Services\Twitch\StreamChangedSubscriptionContract;
use App\Contracts\Services\Twitch\UserFollowsSubscriptionContract;
use App\Contracts\Services\Twitch\UsersInfoContract;
use App\Services\Auth\Login\TwitchLoginService;
use App\Services\Auth\Register\TwitchRegisterService;
use App\Services\Twitch\Subscriptions\StreamChangedSubscriptionService;
use App\Services\Twitch\Subscriptions\UserFollowsSubscriptionService;
use App\Services\Twitch\Users\TwitchUsersInfoService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->isLocal()) {
            $this->app->register(TelescopeServiceProvider::class);
        }

        $this->app->bind(UsersInfoContract::class, TwitchUsersInfoService::class);

        $this->app->bind(TwitchRegisterServiceConract::class, TwitchRegisterService::class);

        $this->app->bind(TwitchLoginServiceContract::class, TwitchLoginService::class);

        $this->app->bind(StreamChangedSubscriptionContract::class, StreamChangedSubscriptionService::class);

        $this->app->bind(UserFollowsSubscriptionContract::class, UserFollowsSubscriptionService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
