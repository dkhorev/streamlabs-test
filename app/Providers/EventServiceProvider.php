<?php

namespace App\Providers;

use App\Events\Twitch\InitiateSubscriptions;
use App\Events\Twitch\TwitchEventsSubscribe;
use App\Listeners\Twitch\SubscribeStreamChanged;
use App\Listeners\Twitch\SubscribeUserFollows;
use App\Listeners\Twitch\StreamerTwitchIdFetch;
use App\Listeners\Users\SubscribeUserToTwitchIdEvents;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            'SocialiteProviders\\Twitch\\TwitchExtendSocialite@handle',
        ],
        TwitchEventsSubscribe::class                          => [
            StreamerTwitchIdFetch::class,
        ],
        InitiateSubscriptions::class                          => [
            SubscribeUserToTwitchIdEvents::class,
            SubscribeUserFollows::class,
            SubscribeStreamChanged::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
