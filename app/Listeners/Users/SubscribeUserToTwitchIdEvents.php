<?php

namespace App\Listeners\Users;

use App\Events\Twitch\InitiateSubscriptions;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscribeUserToTwitchIdEvents implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param InitiateSubscriptions $event
     *
     * @return void
     */
    public function handle(InitiateSubscriptions $event)
    {
        // todo very simplified backend subscription workflow
        $event->user->subscription = $event->twitchId;
        $event->user->save();
    }
}
