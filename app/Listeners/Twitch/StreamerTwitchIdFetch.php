<?php

namespace App\Listeners\Twitch;

use App\Contracts\Services\Twitch\UsersInfoContract;
use App\Events\Twitch\InitiateSubscriptions;
use App\Events\Twitch\StreamerIdReceived;
use App\Events\Twitch\TwitchEventsSubscribe;
use Illuminate\Contracts\Queue\ShouldQueue;

class StreamerTwitchIdFetch implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  TwitchEventsSubscribe $event
     *
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(TwitchEventsSubscribe $event)
    {
        // todo check local repo of twitch logins/ids

        // todo save in local repo and/or use crawler service to have this data always ready
        /* @var UsersInfoContract $service */
        $service = app(UsersInfoContract::class);
        $twitchUser = $service->get($event->username);

        // initiate subscribe event => launch listeners
        event(new InitiateSubscriptions($event->user, $twitchUser->twitch_id));

        // inform client of his channel id
        event(new StreamerIdReceived($event->user, $twitchUser->twitch_id));
    }
}
