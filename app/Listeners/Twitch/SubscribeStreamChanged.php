<?php

namespace App\Listeners\Twitch;

use App\Contracts\Services\Twitch\StreamChangedSubscriptionContract;
use App\Events\Twitch\InitiateSubscriptions;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscribeStreamChanged implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param InitiateSubscriptions $event
     *
     * @return void
     */
    public function handle(InitiateSubscriptions $event)
    {
        /* @var StreamChangedSubscriptionContract $service */
        $service = app(StreamChangedSubscriptionContract::class);
        $service->subscribe($event->twitchId);
    }
}
