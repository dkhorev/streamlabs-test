<?php

namespace App\Listeners\Twitch;

use App\Contracts\Services\Twitch\SubscriptionContract;
use App\Events\Twitch\InitiateSubscriptions;
use App\Services\Twitch\Subscriptions\UserFollowsSubscriptionService;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscribeUserFollows implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param InitiateSubscriptions $event
     *
     * @return void
     */
    public function handle(InitiateSubscriptions $event)
    {
        /* @var SubscriptionContract $service */
        $service = app(UserFollowsSubscriptionService::class);
        $service->subscribe($event->twitchId);
    }
}
