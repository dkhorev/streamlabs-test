<?php

namespace App\Http\Controllers;

use App\Events\Twitch\StreamChangedEventReceived;
use App\Events\Twitch\UserFollowsEventReceived;
use Illuminate\Http\Request;

/**
 * Class TwitchEventsController
 *
 * Handles incoming subscribed events from Twitch
 *
 * @package App\Http\Controllers
 */
class TwitchEventsController extends Controller
{
    /**
     * todo validate request signatue
     *
     * @param Request $request
     * @param string  $twitchId
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    protected function userFollows(Request $request, string $twitchId)
    {
        // todo logic should be in service
        if ($this->isChallengeRequest($request)) {
            return response($request->{'hub_challenge'});
        }

        event(new UserFollowsEventReceived($twitchId, $request->all()));
        // todo end

        return response()->json();
    }

    /**
     * todo validate request signatue
     *
     * @param Request $request
     * @param string  $twitchId
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    protected function streamChanged(Request $request, string $twitchId)
    {
        // todo logic should be in service
        if ($this->isChallengeRequest($request)) {
            return response($request->{'hub_challenge'});
        }

        event(new StreamChangedEventReceived($twitchId, $request->all()));
        // todo end

        return response()->json();
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function isChallengeRequest(Request $request)
    {
        return array_key_exists('hub_challenge', $request->all());
    }
}
