<?php

namespace App\Http\Controllers;

use App\Events\Twitch\TwitchEventsSubscribe;
use App\Http\Requests\WatchStreamRequest;

class StreamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param WatchStreamRequest     $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function index(WatchStreamRequest $request)
    {
        event(new TwitchEventsSubscribe(auth()->user(), $request->username));

        return view('stream', ['username' => $request->username, 'user_id' => auth()->user()->id]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function subscribe()
    {
        return view('subscribe');
    }
}
