<?php

namespace App\Http\Controllers;

use App\Services\Auth\Login\TwitchLoginService;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Laravel\Socialite\Two\InvalidStateException;
use League\OAuth1\Client\Credentials\CredentialsException;

class SocialController extends Controller
{
    /**
     * @var array
     */
    protected $services = [
        'twitch' => TwitchLoginService::class,
    ];

    /**
     * Social login
     *
     * Redirect the user to provider authentication page
     *
     * @param  string $provider
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return (new $this->services[$provider])->redirect();
    }

    /**
     *  Provider response
     *
     * (not for front)
     *
     * @param  string $driver
     *
     * @return JsonResponse|Response
     * @throws \Exception
     */
    public function handleProviderCallback($driver)
    {
        try {
            $user = (new $this->services[$driver])->handle();

            return redirect(route('stream.subscribe'));
        } catch (InvalidStateException $e) {
            return $this->redirectToProvider($driver);
        } catch (ClientException $e) {
            return $this->redirectToProvider($driver);
        } catch (CredentialsException $e) {
            return $this->redirectToProvider($driver);
        }
    }
}
