<?php

namespace App\Broadcasting;

class TwitchEventChannel
{
    /**
     * Authenticate the user's access to the channel.
     *
     * @return bool
     */
    public function join()
    {
        return true;
    }
}
