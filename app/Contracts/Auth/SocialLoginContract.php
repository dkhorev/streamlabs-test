<?php

namespace App\Contracts\Auth;

use App\User;

interface SocialLoginContract
{
    /**
     * @param User   $user
     * @param string $token
     *
     * @return bool
     */
    public function login(User $user, string $token): bool;

    /**
     * @return mixed
     */
    public function redirect();

    /**
     * @return User
     */
    public function handle(): User;
}
