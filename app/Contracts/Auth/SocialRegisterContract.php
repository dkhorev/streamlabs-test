<?php

namespace App\Contracts\Auth;

use App\User;

/**
 * Interface SocialRegisterContract
 *
 * @package App\Contracts\Auth
 */
interface SocialRegisterContract
{
    /**
     * Register by username in app
     *
     * @param string $providerId
     *
     * @return User
     */
    public function store(string $providerId): User;
}
