<?php

namespace App\Contracts\Services\Twitch;

interface SubscriptionContract
{
    public function subscribe(string $twitchId): bool;
}
