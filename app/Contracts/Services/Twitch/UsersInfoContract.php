<?php

namespace App\Contracts\Services\Twitch;

use App\Utils\Object\TwitchUser;

interface UsersInfoContract
{
    /**
     * @param string $username
     *
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @return TwitchUser
     */
    public function get(string $username): TwitchUser;
}
