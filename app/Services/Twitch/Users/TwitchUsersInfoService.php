<?php

namespace App\Services\Twitch\Users;

use App\Contracts\Services\Twitch\UsersInfoContract;
use App\Utils\Object\TwitchUser;
use GuzzleHttp\Client;
use Illuminate\Http\Response;

/**
 * Class TwitchUsersInfoService
 *
 * @package App\Services\Twitch\Users
 */
class TwitchUsersInfoService implements UsersInfoContract
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $headers;

    /**
     * @var string
     */
    protected $uri = 'https://api.twitch.tv/helix/users';

    /**
     * todo extract to abstract class
     *
     * TwitchUsersInfoService constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;

        $this->headers = [
            'Accept'    => 'application/vnd.twitchtv.v5+json',
            'Client-ID' => config('services.twitch.client_id'),
        ];
    }

    /**
     * @param string $username
     *
     * @return TwitchUser
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get(string $username): TwitchUser
    {
        try {
            $response = $this->client->request('GET', $this->uri, [
                'query'       => [
                    'login' => strtolower($username),
                ],
                'headers'     => $this->headers,
                'timeout'     => 5,
            ]);
            $code = $response->getStatusCode();

            if ($code === Response::HTTP_OK) {
                $json = json_decode($response->getBody()->getContents());

                if (is_object($json) === false) {
                    throw new \Exception('Empty response frow twitch');
                }

                $user = collect($json->data)->first();

                return app(TwitchUser::class, ['user' => $user]);
            }

            throw new \Exception('Bad response frow twitch');

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
