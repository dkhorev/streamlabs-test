<?php

namespace App\Services\Twitch\Users;

use GuzzleHttp\Client;
use Illuminate\Http\Response;

/**
 * Class ChannelPastEventsService
 *
 * @package App\Services\Twitch\Users
 */
class ChannelPastEventsService // implements UsersInfoContract
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $headers;

    /**
     * @var string
     */
    protected $uri = 'https://api.twitch.tv/v5/channels/:twitch_id/events';

    /**
     * todo extract to abstract class
     *
     * TwitchUsersInfoService constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;

        $this->headers = [
            'Accept'    => 'application/vnd.twitchtv.v5+json',
            'Client-ID' => config('services.twitch.client_id'),
        ];
    }

    /**
     * @param string $twitchId
     *
     * @return mixed
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get(string $twitchId)
    {
        try {
            $response = $this->client->request(
                'GET',
                str_replace(':twitch_id', $twitchId, $this->uri),
                [
                    'headers' => $this->headers,
                    'timeout' => 5,
                ]
            );
            $code = $response->getStatusCode();

            if ($code === Response::HTTP_OK) {
                $json = json_decode($response->getBody()->getContents());

                if (is_object($json) === false) {
                    throw new \Exception('Empty response frow twitch');
                }

                return $json;
            }

            throw new \Exception('Bad response frow twitch');
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
