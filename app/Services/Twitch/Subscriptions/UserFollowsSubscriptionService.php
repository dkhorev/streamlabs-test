<?php

namespace App\Services\Twitch\Subscriptions;

use App\Contracts\Services\Twitch\UserFollowsSubscriptionContract;

class UserFollowsSubscriptionService extends AbstractSubscriptionService implements UserFollowsSubscriptionContract
{
    protected $callback = 'in.events.user-follows';
    protected $topic = 'https://api.twitch.tv/helix/users/follows?first=1&to_id=';
}
