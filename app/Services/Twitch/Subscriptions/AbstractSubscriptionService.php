<?php

namespace App\Services\Twitch\Subscriptions;

use App\Contracts\Services\Twitch\SubscriptionContract;
use GuzzleHttp\Client;
use Illuminate\Http\Response;

/**
 * Class AbstractSubscriptionService
 *
 * @package App\Services\Twitch\Subscriptions
 */
abstract class AbstractSubscriptionService implements SubscriptionContract
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $headers;

    /**
     * @var string
     */
    protected $method = 'POST';

    /**
     * @var string
     */
    protected $uri = 'https://api.twitch.tv/helix/webhooks/hub';

    /**
     * @var string
     */
    protected $callback;

    /**
     * @var string
     */
    protected $topic;

    /**
     * TwitchUsersInfoService constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;

        $this->headers = [
            'Accept'    => 'application/vnd.twitchtv.v5+json',
            'Client-ID' => config('services.twitch.client_id'),
        ];
    }

    /**
     * @param string $twitchId
     *
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function subscribe(string $twitchId): bool
    {
        try {
            $response = $this->client->request($this->method, $this->uri, [
                'headers' => $this->headers,
                'query'   => [
                    'hub.callback'      => route($this->callback, ['twitchId' => $twitchId]),
                    'hub.mode'          => 'subscribe',
                    'hub.topic'         => $this->topic . $twitchId,
                    // todo sub limit is 10 minutes for testing only
                    'hub.lease_seconds' => '600',
                    'hub.secret'        => 'secret123',
                ],
                'timeout' => 5,
            ]);
            $code = $response->getStatusCode();

            if ($code === Response::HTTP_ACCEPTED) {
                return true;
            }

            throw new \Exception('Bad response frow twitch');
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
