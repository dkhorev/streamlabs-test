<?php

namespace App\Services\Twitch\Subscriptions;

use App\Contracts\Services\Twitch\StreamChangedSubscriptionContract;

/**
 * Class StreamChangedSubscriptionService
 *
 * @package App\Services\Twitch\Subscriptions
 */
class StreamChangedSubscriptionService extends AbstractSubscriptionService implements StreamChangedSubscriptionContract
{
    protected $callback = 'in.events.stream-changed';
    protected $topic = 'https://api.twitch.tv/helix/streams?user_id=';
}
