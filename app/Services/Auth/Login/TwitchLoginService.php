<?php

namespace App\Services\Auth\Login;

use App\Contracts\Auth\TwitchLoginServiceContract;
use App\Contracts\Auth\TwitchRegisterServiceConract;
use App\User;

/**
 * Class TwitchLoginService
 *
 * @package App\Services\Auth\Login
 */
class TwitchLoginService extends AbstractLoginService implements TwitchLoginServiceContract
{
    /**
     * @var string
     */
    protected $provider = 'twitch';

    /**
     * @return User
     * @throws \Exception
     */
    public function handle(): User
    {
        $userSocial = $this->providerService->user();

        // todo change to UserRepo
        $user = User::where('provider', $this->provider)
                    ->where('provider_id', $userSocial->getId())
                    ->get()
                    ->first();

        if (!$user) {
            $user = app(TwitchRegisterServiceConract::class)->store($userSocial->getId());
        }

        $this->login($user, $userSocial->token);

        return $user;
    }
}
