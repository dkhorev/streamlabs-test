<?php

namespace App\Services\Auth\Login;

use App\Contracts\Auth\SocialLoginContract;
use App\Services\Auth\SocialService;
use App\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

abstract class AbstractLoginService extends SocialService implements SocialLoginContract
{
    protected $providerService;

    /**
     *  Create a new AbstractLoginService instance
     */
    public function __construct()
    {
        $this->providerService = Socialite::with($this->provider);
    }

    /**
     * @param User   $user
     * @param string $token
     *
     * @return bool
     */
    public function login(User $user, string $token): bool
    {
        $user->api_token = $token;
        $user->save();

        return Auth::loginUsingId($user->id, true) ? true : false;
    }

    /**
     *  Redirect the user to provider authentication page
     *
     * @return \Illuminate\Http\Response
     */
    public function redirect()
    {
        return $this->providerService->redirect();
    }
}
