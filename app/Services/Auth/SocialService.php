<?php

namespace App\Services\Auth;

/**
 * Class SocialService
 *
 * @package App\Services\Auth
 */
abstract class SocialService
{
    /**
     * @var
     */
    protected $provider;

    /**
     * @var
     */
    protected $providerShort;

    /**
     * todo custom exceptions
     *
     * @throws \Exception
     */
    protected function checkServiceSetup()
    {
        if (!$this->provider) {
            throw new \Exception('provider is not set');
        }

        if (!$this->providerShort) {
            throw new \Exception('providerShort is not set');
        }
    }
}
