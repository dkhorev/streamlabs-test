<?php

namespace App\Services\Auth\Register;

use App\Contracts\Auth\SocialRegisterContract;
use App\Services\Auth\SocialService;
use App\User;
use Illuminate\Support\Str;

abstract class AbstractRegisterService extends SocialService implements SocialRegisterContract
{
    /**
     * Register by username in app
     *
     * todo work with UserRepo instead of User model
     *
     * @param string $providerId
     *
     * @return User
     * @throws \Exception
     */
    public function store(string $providerId): User
    {
        $this->checkServiceSetup();

        $data = [
            'login'       => $this->generateLogin($providerId),
            'password'    => bcrypt(Str::random(20)),
            'provider'    => $this->provider,
            'provider_id' => $providerId,
        ];

        return User::create($data);
    }

    /**
     * @param        $id
     *
     * @return string
     */
    protected function generateLogin($id)
    {
        return $this->providerShort . '-' . $id;
    }
}
