<?php

namespace App\Services\Auth\Register;

use App\Contracts\Auth\TwitchRegisterServiceConract;

/**
 * Class TwitchRegisterService
 *
 * @package App\Services\Auth\Register
 */
class TwitchRegisterService extends AbstractRegisterService implements TwitchRegisterServiceConract
{
    protected $provider = 'twitch';
    protected $providerShort = 'twtch';
}
