<?php

namespace App\Utils\Object;

/**
 * Class TwitchUser
 *
 * @property string $twitch_id
 * @property string $login
 * @package App\Utils\Object
 */
class TwitchUser
{
    public $twitch_id;
    public $login;

    public function __construct($user)
    {
        $this->twitch_id = $user->id;
        $this->login = $user->login;
    }
}
