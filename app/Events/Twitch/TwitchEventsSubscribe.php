<?php

namespace App\Events\Twitch;

use App\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TwitchEventsSubscribe
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var User|null
     */
    public $user;

    /**
     * @var string
     */
    public $username;

    /**
     * Create a new event instance.
     *
     * @param Authenticatable|null $user
     * @param string               $username
     */
    public function __construct(?Authenticatable $user, string $username)
    {
        $this->user = $user;
        $this->username = $username;
    }
}
