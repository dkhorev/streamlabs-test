<?php

namespace App\Events\Twitch;

use App\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class InitiateSubscriptions
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Authenticatable|User|null
     */
    public $user;

    /**
     * @var string
     */
    public $twitchId;

    /**
     * Create a new event instance.
     *
     * @param Authenticatable|null $user
     * @param string               $twitchId
     */
    public function __construct(?Authenticatable $user, string $twitchId)
    {
        $this->user = $user;
        $this->twitchId = $twitchId;
    }
}
