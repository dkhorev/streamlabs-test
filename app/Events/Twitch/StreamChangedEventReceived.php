<?php

namespace App\Events\Twitch;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class StreamChangedEventReceived
 *
 * @package App\Events\Twitch
 */
class StreamChangedEventReceived implements ShouldBroadcast, ShouldQueue
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $twitchId;

    /**
     * @var array
     */
    public $data;

    /**
     * Create a new event instance.
     *
     * @param string $twitchId
     * @param array  $data
     */
    public function __construct(string $twitchId, array $data)
    {
        $this->twitchId = $twitchId;
        $this->data = collect($data['data'])->first();

        if (count($this->data) === 0) {
            // todo must be separate StreamOfflineEvent
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('TwitchStreamChangedEvent.' . $this->twitchId);
    }
}
