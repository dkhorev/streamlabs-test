<?php

namespace Tests\Feature\Api;

use App\Events\Twitch\StreamChangedEventReceived;
use App\Events\Twitch\UserFollowsEventReceived;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class TwitchEventsControllerTest extends TestCase
{
    public function eventRoutes()
    {
        return [
            ['in.events.user-follows'],
            ['in.events.stream-changed'],
        ];
    }

    /** @test */
    public function testInstantiateUserFollowsEventReceived()
    {
        $data = ['data' => ['to_id' => '123']];

        $event = app(
            UserFollowsEventReceived::class,
            ['twitchId' => '123', 'data' => $data]
        );

        $this->assertInstanceOf(UserFollowsEventReceived::class, $event);
        $this->assertInstanceOf(ShouldBroadcast::class, $event);
        $this->assertInstanceOf(ShouldQueue::class, $event);
    }

    /** @test */
    public function testInstantiateStreamChangedEventReceived()
    {
        $event = app(
            StreamChangedEventReceived::class,
            ['twitchId' => '123', 'data' => ['data' => [['to_id' => '123']]]]
        );

        $this->assertInstanceOf(StreamChangedEventReceived::class, $event);
        $this->assertInstanceOf(ShouldBroadcast::class, $event);
        $this->assertInstanceOf(ShouldQueue::class, $event);
    }

    /**
     * @test
     * @dataProvider eventRoutes
     *
     * @param $route
     */
    public function testAcceptsTwitchWebhookVerification($route)
    {
        // act
        $response = $this->json('GET', route($route, ['twitchId' => '333']), [
            'hub_mode'          => 'subscribe',
            'hub_topic'         => 'https://api.twitch.tv/helix/users/follows?first=1&to_id=333',
            'hub_lease_seconds' => '864000',
            'hub_challenge'     => 'HzSGH_h04Cgl6VbDJm7IyXSNSlrhaLvBi9eft3bw',
        ]);

        // assert
        $response->assertStatus(Response::HTTP_OK);
        $this->assertEquals('HzSGH_h04Cgl6VbDJm7IyXSNSlrhaLvBi9eft3bw', $response->getContent());
    }

    /** @test */
    public function testUserFollowsAcceptsDataForBroadcasting()
    {
        Event::fake();
        $this->withoutExceptionHandling();
        // act
        $response = $this->json('GET', route('in.events.user-follows', ['twitchId' => '1337']), [
            'data' => [[
                'from_id'     => '1336',
                'from_name'   => 'ebi',
                'to_id'       => '1337',
                'to_name'     => 'oliver0823nagy',
                'followed_at' => '2017-08-22T22:55:24Z',
            ]],
        ]);

        // assert
        $response->assertStatus(Response::HTTP_OK);
        Event::assertDispatched(UserFollowsEventReceived::class, function (UserFollowsEventReceived $event) {
            return $event->data['from_id'] === '1336'
                && $event->data['from_name'] === 'ebi'
                && $event->data['to_id'] === '1337'
                && $event->data['to_name'] === 'oliver0823nagy'
                && $event->data['followed_at'] === '2017-08-22T22:55:24Z'
                && $event->twitchId === '1337';
        });
    }

    /** @test */
    public function testStreamChangedAcceptsDataForBroadcasting()
    {
        Event::fake();
        $this->withoutExceptionHandling();
        // act
        $response = $this->json('GET', route('in.events.stream-changed', ['twitchId' => '5678']), [
            'data' => [[
                'id'            => '0123456789',
                'user_id'       => '5678',
                'user_name'     => 'wjdtkdqhs',
                'game_id'       => '21779',
                'community_ids' => [],
                'type'          => 'live',
                'title'         => 'Best Stream Ever',
                'viewer_count'  => '417',
                'started_at'    => '2017-12-01T10:09:45Z',
                'language'      => 'en',
                'thumbnail_url' => 'https://link/to/thumbnail.jpg',
            ]],
        ]);

        // assert
        $response->assertStatus(Response::HTTP_OK);
        Event::assertDispatched(StreamChangedEventReceived::class, function (StreamChangedEventReceived $event) {
            return $event->data['id'] === '0123456789'
                && $event->data['user_id'] === '5678'
                && $event->twitchId === '5678';
        });
    }
}
