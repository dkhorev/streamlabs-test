<?php

namespace Tests\Unit\Services\Twitch\Subscriptions;

use App\Contracts\Services\Twitch\SubscriptionContract;
use App\Contracts\Services\Twitch\UserFollowsSubscriptionContract;
use App\Services\Twitch\Subscriptions\UserFollowsSubscriptionService;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class UserFollowsSubscriptionServiceTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /* @var SubscriptionContract $service */
        $service = app(UserFollowsSubscriptionService::class);

        $this->assertInstanceOf(UserFollowsSubscriptionService::class, $service);
        $this->assertInstanceOf(UserFollowsSubscriptionContract::class, $service);
        $this->assertInstanceOf(SubscriptionContract::class, $service);
    }

    /** @test */
    public function testCanSubscribe()
    {
        // mock guzzle
        $this->app->bind(Client::class, function () {
            $mock = new MockHandler([
                new Response(202, [], json_encode([])),
            ]);

            $handler = HandlerStack::create($mock);

            return new Client(['handler' => $handler]);
        });

        /* @var SubscriptionContract $service */
        $service = app(UserFollowsSubscriptionService::class);
        $result = $service->subscribe('28633177');

        $this->assertTrue($result);
    }
}
