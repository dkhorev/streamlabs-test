<?php

namespace Tests\Unit\Services\Twitch\Subscriptions;

use App\Contracts\Services\Twitch\StreamChangedSubscriptionContract;
use App\Contracts\Services\Twitch\SubscriptionContract;
use App\Services\Twitch\Subscriptions\StreamChangedSubscriptionService;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class StreamChangedSubscriptionServiceTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /* @var SubscriptionContract $service */
        $service = app(StreamChangedSubscriptionService::class);

        $this->assertInstanceOf(StreamChangedSubscriptionService::class, $service);
        $this->assertInstanceOf(StreamChangedSubscriptionContract::class, $service);
        $this->assertInstanceOf(SubscriptionContract::class, $service);
    }

    /** @test */
    public function testCanSubscribe()
    {
        // mock guzzle
        $this->app->bind(Client::class, function () {
            $mock = new MockHandler([
                new Response(202, [], json_encode([])),
            ]);

            $handler = HandlerStack::create($mock);

            return new Client(['handler' => $handler]);
        });

        /* @var SubscriptionContract $service */
        $service = app(StreamChangedSubscriptionService::class);
        $result = $service->subscribe('28633177');

        $this->assertTrue($result);
    }

}
