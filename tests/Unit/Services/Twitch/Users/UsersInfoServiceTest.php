<?php

namespace Tests\Unit\Services\Twitch\Users;

use App\Contracts\Services\Twitch\UsersInfoContract;
use App\Services\Twitch\Users\TwitchUsersInfoService;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class UsersInfoServiceTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /* @var UsersInfoContract $service */
        $service = app(TwitchUsersInfoService::class);

        $this->assertInstanceOf(TwitchUsersInfoService::class, $service);
        $this->assertInstanceOf(UsersInfoContract::class, $service);
    }

    /** @test
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testGetsUserInfoFromTwitch()
    {
        // mock guzzle
        $this->app->bind(Client::class, function () {
            $mock = new MockHandler([
                new Response(200, [], json_encode([
                    'data' => [
                        [
                            'id'    => '28633177',
                            'login' => 'starladder1',
                        ],
                    ],
                ])),
            ]);

            $handler = HandlerStack::create($mock);

            return new Client(['handler' => $handler]);
        });

        /* @var UsersInfoContract $service */
        $service = app(TwitchUsersInfoService::class);
        $object = $service->get('starladder1');

        // assert
        $this->assertEquals('28633177', $object->twitch_id);
        $this->assertEquals('starladder1', $object->login);
    }
}
