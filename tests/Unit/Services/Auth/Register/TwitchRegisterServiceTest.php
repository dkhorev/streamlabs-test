<?php

namespace Tests\Unit\Services\Auth\Register;

use App\Contracts\Auth\SocialRegisterContract;
use App\Contracts\Auth\TwitchRegisterServiceConract;
use App\Services\Auth\Register\TwitchRegisterService;
use Tests\TestCase;

class TwitchRegisterServiceTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /* @var SocialRegisterContract $service */
        $service = app(TwitchRegisterService::class);

        $this->assertInstanceOf(TwitchRegisterService::class, $service);
        $this->assertInstanceOf(TwitchRegisterServiceConract::class, $service);
        $this->assertInstanceOf(SocialRegisterContract::class, $service);
    }

    /** @test */
    public function testRegistersCorrectly()
    {
        /* @var SocialRegisterContract $service */
        $service = app(TwitchRegisterService::class);

        // act
        $user = $service->store('id123~111');

        // assert
        $this->assertEquals('id123~111', $user->provider_id);
    }
}
