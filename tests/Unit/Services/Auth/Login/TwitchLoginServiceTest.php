<?php

namespace Tests\Unit\Services\Auth\Login;

use App\Contracts\Auth\SocialLoginContract;
use App\Contracts\Auth\SocialRegisterContract;
use App\Contracts\Auth\TwitchLoginServiceContract;
use App\Services\Auth\Login\TwitchLoginService;
use App\Services\Auth\Register\TwitchRegisterService;
use Tests\TestCase;

class TwitchLoginServiceTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /* @var SocialLoginContract $service */
        $service = app(TwitchLoginService::class);

        $this->assertInstanceOf(TwitchLoginService::class, $service);
        $this->assertInstanceOf(TwitchLoginServiceContract::class, $service);
        $this->assertInstanceOf(SocialLoginContract::class, $service);
    }

    /** @test */
    public function testLogin()
    {
        // setup
        /* @var SocialRegisterContract $service */
        $service = app(TwitchRegisterService::class);
        $user = $service->store('id123~111');

        /* @var SocialLoginContract $service */
        $service = app(TwitchLoginService::class);
        // act
        $service->login($user, 'token123');

        $this->assertAuthenticatedAs($user);
    }
}
