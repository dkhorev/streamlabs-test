<?php

namespace Tests\Unit\Listeners\Users;

use App\Events\Twitch\InitiateSubscriptions;
use App\Listeners\Users\SubscribeUserToTwitchIdEvents;
use App\User;
use Tests\TestCase;

class SubscribeUserToTwitchIdEventsTest extends TestCase
{
    /** @test */
    public function testSavesTwitchIdToListenTo()
    {
        /** @var User $user */
        $user = factory(User::class)->create();

        /** @var SubscribeUserToTwitchIdEvents $listener */
        $listener = app(SubscribeUserToTwitchIdEvents::class);

        // act
        $listener->handle(new InitiateSubscriptions($user, '123'));

        // assert
        $this->assertEquals('123', $user->subscription);
    }
}
