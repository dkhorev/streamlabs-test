pipeline {
    agent any

    environment {
        APP_ENV = 'local'
        APP_DEBUG = 'true'
        APP_URL = 'https://streamlabs.dkhorev.pro'
        MIX_PUSHER_APP_KEY = credentials('pusher-app-key')
        MIX_PUSHER_APP_CLUSTER = 'eu'
    }

    stages {
      stage('Git') {
        steps {
          slackSend (color: 'good', message: "build #${env.BUILD_ID} [${env.JOB_NAME}] started.")
          checkout scm
        }
      }

      stage('Test') {
        steps {
          // todo - no tests for demo CI/CD, need more complex setup
          sh 'composer install --ignore-platform-reqs --no-interaction'
          // sh 'composer test'
          // sh 'composer install --ignore-platform-reqs --no-interaction --no-plugins --no-scripts --prefer-dist --no-dev'
          sh 'npm install'
          sh 'npm run prod'
        }
      }

      stage('Build') {
        steps {
          script {
           docker.withRegistry('https://docker.p2pwatch.io', 'docker-pass') {
              def front = docker.build("sev-app:latest", "--rm -f app.dockerfile .")
              front.push('latest')
              def web = docker.build("sev-web:latest", "--rm -f web.dockerfile .")
              web.push('latest')
           }
          }
        }
      }

      stage('Deploy') {
        steps {
          sh 'ansible-playbook playbook.yml'
        }
      }

      stage ('Clean') {
        steps {
          sh 'docker rmi --force $(docker images -f dangling=true -q)'
          cleanWs()
        }
      }
    }

    post {
        success {
          slackSend (color: 'good', message: "build #${env.BUILD_ID} [${env.JOB_NAME}] complete!")
        }
        failure {
          slackSend (color: 'bad', message: "build #${env.BUILD_ID} [${env.JOB_NAME}] failed!")
        }
    }
}