#!/usr/bin/env bash

docker swarm init --advertise-addr=159.89.27.106

env $(cat .env.docker | grep ^[A-Z] | xargs) docker stack deploy --with-registry-auth --compose-file docker-compose-prod.yml sev
