<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('login')->after('id')->unique();
            $table->string('provider')->after('login');
            $table->string('provider_id')->after('provider');
            $table->string('api_token')->after('provider_id')->nullable();

            $table->unique(['provider', 'provider_id']);

            $table->string('name')->nullable()->change();
            $table->string('email')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropUnique(['provider', 'provider_id']);
            $table->dropColumn(['provider', 'provider_id', 'api_token', 'login']);

            $table->string('name')->nullable(false)->change();
            $table->string('email')->nullable(false)->change();
        });
    }
}
