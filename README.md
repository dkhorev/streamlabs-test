# streamlabs test project

Test project for streamlabs.com

# StreamerEventViewer (SEV)

## Introduction

In this coding assignment, you will be building a backend web application which will help its audience see their favorite streamer's Twitch events in real-time. Detailed specifications along with some other helpful information is provided below. Goodluck!

## How to approach this and logistics

You are not expected to (cumulatively) spend more than *4* hours on this task. These 4 hours can be spread over multiple days within a two week timeframe. You can start whenever you like. Please make reasonable assumptions and design choices where you feel the specification is unclear or falls short. Make sure to document your assumptions and design in the `README` or other relevant documents you produce (e.g. code comments).

To help you make these decisions, keep in mind that we ask candidates to complete these coding assignments in order to:

1. Gauge your technical strength and ability to work independently
2. Ability to navigate third-party interfaces (libraries/apis) where the documentation/spec might not be very detailed

All work you do will be your property; we are not trying to get free work done here. A couple hundred people have done this assignment previously (some are part of our team now!) and this functionality is live in our product(s).

We are expecting two things from you:
1. Link to live demo (heroku tends to be pretty easy/free and is used by most people)
2. Link to git repo (Github is a famous choice) consisting your source and `README`


## Specifications

0. You are required to build an application which exposes two simple pages to the browser

1. The first/home page lets a user login with Twitch and set their favorite Twitch streamer name. This initiates a backend event
   listener which listens to all events for given streamer.

2. The second/streamer page shows an embedded livestream, chat and list of 10 most recent events for your favorite streamer. This page
   doesn’t poll the backend and rather leverages web sockets and relevant Twitch API.

3. Additionally please answer following questions at the bottom of your `README`:
- How would you deploy the above on AWS? (ideally a rough architecture diagram will help)
- Where do you see bottlenecks in your proposed architecture and how would you approach scaling this app starting from 100 reqs/day to
   900MM reqs/day over 6 months?

> Goodluck! And please feel free to ping us if you have any questions. We looking forward to having you on-board our team :)

## Result

Git repo: [https://gitlab.com/dkhorev/streamlabs-test](https://gitlab.com/dkhorev/streamlabs-test)

`Warning: if you have blocked third-party cookies in your browser - you must enable them for testing (Chrome: chrome://settings/content/cookies). Otherwise there will be problems with embedded chat producing javascript errors and breaking the page.`

## Design choises

1. I am using webhook API for event listening. I beleive it's new so not all events are supported. App subscribes to: UserFollows, StreamChanged. Mainly you will get UserFollows - so choose some active stream for testing.

`The other way was to use Pubsub API, but this would take much longer to implement server side (find package to work on backend or write a new one, a lot of events with different data etc, javascript on client is possibility - but this can hit topic limit quite fast)`

2. Built with TDD :) For running test you need to setup real "test" DB (edit .env.testing). Run `php artisan migrate --env=testing`.

`I am doing real DB over sqlite because this is closer to real world testing and not much slower. Sqlite has it's limitations like foreign keys off by default, not casting integer fields and maybe something else.` 

3. Uses pusher.com to broadcast (free and integrated with laravel). Laravel Echo on front-end (easy integration).

4. Was not able to fetch "last 10 events". I found a route mention 'https://api.twitch.tv/v5/channels/:twitch_id/events', but this one always returns empty list even on very active streams:
```
{
  +"_total": 0
  +"events": []
}
```

## Design limitations (lack of time to implement, find by "todo" tag in code)

No check if streamer nickname exists before redirecting to player page

StreamOfflineEvent for StreamChanged not supported (probably will be an error on backend queue)

Webhooks dont validate request signature (TwitchEventsController)

Webhook controller logic not abstracted to services like most app (TwitchEventsController)

Highly dependend on DB layer when working with User, no UserRepository is made to abstract it.

Known twitch user_id could cached to reduce number of twitch API requests (StreamerTwitchIdFetch)

Very simplified "subscription to events" workflow (a better way is to use relation table) (SubscribeUserToTwitchIdEvents)

No custom exceptions

Subscription time limit is hardcoded for testing (10mins)

Some services can have some methods extracted to abstract parent class (TwitchUsersInfoService)

No unsubscribe jobs on backend when everyone leaves events channel

Front-end part with js is simplified for faster development

## Deploying to AWS

This demo project has CI/CD scripts with Docker, Docker Swarm, Jenkins and Ansible. Deployed to Digital Ocean server.

I have no prior experience with deploying to AWS, but I'm sure it is possible with same toolset on EC2 instance. Also I see they have specialized instances for docker, CI/CD - so larger deploy systems can be made aswell.

## Scaling the app

Here are some steps we can make to scale this app:
1. Make caching of twich user_ids we subscribe to => Save them to our own DB => Make crawler to fill this DB with user_ids,username pairs (or other needed data).
2. Put up own socket server to reduce latency and costs.
3. App is built with plugin architecture in mind. Each service or handler has it's contract. This makes every module independent of each other. I.e. any service can run on separate app, then you scale that app behind a load balancer etc.
4. Use nosql DB/caching

